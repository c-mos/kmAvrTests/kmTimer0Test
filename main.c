/*
 * kmTimer0Test.c
 *
 * Created: 7/18/2022 5:12:46 PM
 * Author : km
 */ 

#include <stdbool.h>
#include "Application.h"

int main(void) {

	appInit();
	
	while (true) {
		appLoop();
	}
}
