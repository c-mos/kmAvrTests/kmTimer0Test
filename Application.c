/*
 * Application.c
 *
 *  Created on: Jan 16, 2020
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmAvrLedBar application for controlling WS281X chain of LEDs
 *  Copyright (C) 2020  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"


#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer0/kmTimer0.h"

#define KM_TIMER0_TEST_1MS 1000 // number of microseconds defining 1 millisecond
#define KM_TIMER0_TEST_5MS 5000 // number of microseconds defining 5 millisecond
#define KM_TIMER0_TEST_USER_DATA_A 1UL
#define KM_TIMER0_TEST_USER_DATA_B 65535UL
#define KM_TIMER0_TEST_USER_DATA_C 255UL
#define KM_TIMER0_TEST_DUTY_0_PERC KM_TIMER0_BOTTOM
#define KM_TIMER0_TEST_DUTY_25_PERC KM_TIMER0_MID - (KM_TIMER0_MID >> 1)
#define KM_TIMER0_TEST_DUTY_50_PERC KM_TIMER0_MID
#define KM_TIMER0_TEST_DUTY_75_PERC KM_TIMER0_MID + (KM_TIMER0_MID >> 1)
#define KM_TIMER0_TEST_DUTY_100_PERC KM_TIMER0_MAX

#define KM_TIMER0_TEST_PHASE_180_DEG KM_TIMER0_MID

// "private" variables
static const uint8_t swipeTestValues[] = { 0, 1, 16, 32, 64, 128, 192, 250, 255, 254, 128 };

// "private" functions
uint8_t getSwipeTestRange(void) {
	return sizeof(swipeTestValues) / sizeof(swipeTestValues[0]);
}

void callbackOVF(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
	dbOn(DB_PIN_1);
	dbOn(DB_PIN_2);
	dbOn(DB_PIN_3);
}

void callbackOVFToggle(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
}

void callbackCompAToggle(void *userData) {
	//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_1);
}

void callbackCompBToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_2);
}


void callbackCompAOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_2);
}

void testSwipeCompATable(void) {
	uint8_t swipeRange = getSwipeTestRange();
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer0SetValueCompA(swipeTestValues[i]);
	}
}

void testSwipeCompA(void) {
	for (uint16_t i = 0; i < KM_TIMER0_MAX - KM_TIMER0_TEST_SWIPE_ACCURACY; i += KM_TIMER0_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer0SetValueCompA(i);
	}
}

#ifndef OCR0
void testSwipeCompBTable(void) {
	uint8_t swipeRange = getSwipeTestRange();
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer0SetValueCompB(swipeTestValues[i]);
	}
}

void testSwipeCompB(void) {
	for (int i = 0; i < KM_TIMER0_MAX; i++) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
		kmTimer0SetValueCompB(i);
		dbToggle(DB_PIN_4);
	}
}
#endif /* OCR0 */

void testSwipePwm(const Tcc0PwmOut pwmOut, uint8_t maxValue) {
	kmTimer0SetPwmInversion(pwmOut, false);

	for (int i = 0; i <= maxValue; i++) {
		kmTimer0SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);		
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
	}
	
	kmTimer0SetPwmInversion(pwmOut, true);
	for (int i = maxValue; i >= 0; i--) {
		kmTimer0SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
	}
}

#ifndef OCR0
void testSwipePwmAccurateTime(uint8_t maxValue) {
	kmTimer0SetPwmInversion(KM_TCC0_PWM_OUT_B, false);
	
	for (int i = 0; i <= maxValue; i++) {
		kmTimer0SetPwmDutyAccurateTimeModes(i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
	}
	
	kmTimer0SetPwmInversion(KM_TCC0_PWM_OUT_B, true);
	for (int i = maxValue; i >= 0; i--) {
		kmTimer0SetPwmDutyAccurateTimeModes(i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER0_TEST_SWIPE_DELAY);
	}
}
#endif /* OCR0 */

void testSwipePwmTable(const Tcc0PwmOut pwmOut) {
	uint8_t swipeRange = getSwipeTestRange();

	kmTimer0SetPwmInversion(pwmOut, false);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer0SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
	
	kmTimer0SetPwmInversion(pwmOut, true);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER0_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer0SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
}

void appTest0(void) {
	kmTimer0InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER0_TEST_1MS);
	kmTimer0Start();
}

void appTest1(void) {
	kmTimer0InitOnAccurateTimeCompAInterruptCallback(KM_TIMER0_TEST_5MS, true);

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();
}

void appTest2(void) {
	kmTimer0InitOnPrescalerBottomToTopOvfCompABInterruptCallback(KM_TCC0_PRSC_8);

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer0SetValueCompA(KM_TIMER0_TEST_DUTY_25_PERC);
	kmTimer0EnableInterruptCompA();

#ifndef OCR0
	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer0SetValueCompB(KM_TIMER0_TEST_DUTY_75_PERC);
	kmTimer0EnableInterruptCompB();
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0ConfigureOCA(KM_TCC0_A_COMP_OUT_TOGGLE);
#ifndef OCR0
	kmTimer0ConfigureOCB(KM_TCC0_B_COMP_OUT_TOGGLE);
#endif /* OCR0 */
	kmTimer0Start();

#ifdef KM_TIMER0_TEST_SWIPE
	testSwipeCompA();
#ifndef OCR0
	testSwipeCompB();
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_SWIPE */
}

void appTest3(void) {
#ifndef OCR0
	kmTimer0InitOnAccurateTimeCompABInterruptCallback(KM_TIMER0_TEST_1MS, KM_TIMER0_TEST_PHASE_180_DEG); // for 1 millisecond

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();
#endif /* OCR0 */
}

void appTest4(void) {
#ifndef OCR0
	kmTimer0InitOnPrescalerBottomToTopFastPwm(KM_TCC0_PRSC_64, KM_TIMER0_TEST_DUTY_25_PERC, false, KM_TIMER0_TEST_DUTY_75_PERC, false);
#else /* OCR0 */
	kmTimer0InitOnPrescalerBottomToTopFastPwmOCA(KM_TCC0_PRSC_64, KM_TIMER0_MID, false);
#endif /* OCR0 */

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

#ifndef OCR0
	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();

#ifdef KM_TIMER0_TEST_SWIPE
	testSwipePwm(KM_TCC0_PWM_OUT_A, KM_TIMER0_MAX);
	kmTimer0SetValueCompA(KM_TIMER0_MID);
#ifndef OCR0
	testSwipePwm(KM_TCC0_PWM_OUT_B, KM_TIMER0_MAX);
	kmTimer0SetValueCompB(KM_TIMER0_MID);
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_SWIPE */
}

void appTest5(void) {
#ifndef OCR0
	uint8_t cyclesRange = kmTimer0InitOnAccurateTimeFastPwm(KM_TIMER0_TEST_1MS, true, KM_TIMER0_TEST_DUTY_25_PERC, false);

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();

#ifdef KM_TIMER0_TEST_SWIPE
	// to change duty on the channel B
	testSwipePwmAccurateTime(cyclesRange);
	kmTimer0SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* KM_TIMER0_TEST_SWIPE */
#endif /* OCR0 */
}

void appTest6(void) {
#ifndef OCR0
	kmTimer0InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_64, KM_TIMER0_TEST_DUTY_25_PERC, false, KM_TIMER0_TEST_DUTY_75_PERC, false);
#else /* OCR0 */
	kmTimer0InitOnPrescalerBottomToTopPcPwmOCA(KM_TCC0_PRSC_64, KM_TIMER0_MID, false);
#endif /* OCR0 */

#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVF);

	kmTimer0EnableInterruptCompA();
	kmTimer0EnableInterruptOVF();

#ifndef OCR0
	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBOff);
	kmTimer0EnableInterruptCompB();
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();

#ifdef KM_TIMER0_TEST_SWIPE
	testSwipePwm(KM_TCC0_PWM_OUT_A, KM_TIMER0_MAX);
	kmTimer0SetValueCompA(KM_TIMER0_MID);
#ifndef OCR0
	testSwipePwm(KM_TCC0_PWM_OUT_B, KM_TIMER0_MAX);
	kmTimer0SetValueCompB(KM_TIMER0_MID);
#endif /* OCR0 */
#endif /* KM_TIMER0_TEST_SWIPE */
}

void appTest7(void) {
#ifndef OCR0
	kmTimer0InitOnAccurateTimePcPwm(KM_TIMER0_TEST_1MS, true, KM_TIMER0_TEST_DUTY_25_PERC, false);
	
#ifdef KM_TIMER0_TEST_INTERRUPTS
	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();
#endif /* KM_TIMER0_TEST_INTERRUPTS */

	kmTimer0Start();

#ifdef KM_TIMER0_TEST_SWIPE
	testSwipePwmAccurateTime(KM_TIMER0_MAX);
	kmTimer0SetValueCompB(KM_TIMER0_MID);
#endif /* KM_TIMER0_TEST_SWIPE */
#endif /* OCR0 */
}

void appTest8(void) {
	kmTimer0Init(KM_TCC0_PRSC_1024, KM_TCC0_MODE_3_A, KM_TCC0_MODE_3_B);

	kmTimer0SetValueCompA(4u);
	kmTimer0ConfigureOCA(KM_TCC0_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);

#ifndef OCR0
	kmTimer0SetValueCompB(9u);
	kmTimer0ConfigureOCB(KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR0 */

	kmTimer0Start();
}

void appInitDebug(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);
}

void appInit(void) {
	kmCpuDisableWatchdogAndInterruptsOnStartup();
	appInitDebug();
	kmCpuInterruptsEnable();

	static const uint8_t testNumber = KM_TIMER0_TEST_NUMBER;

	switch (testNumber) {
		case 0: {
			appTest0();
			break;
		}
		case 1: {
			appTest1();
			break;
		}
		case 2: {
			appTest2();
			break;
		}
		case 3: {
			appTest3();
			break;
		}
		case 4: {
			appTest4();
			break;
		}
		case 5: {
			appTest5();
			break;
		}
		case 6: {
			appTest6();
			break;
		}
		case 7: {
			appTest7();
			break;
		}
		case 8: {
			appTest8();
			break;
		}
	}
}

void appLoop(void) {
}
